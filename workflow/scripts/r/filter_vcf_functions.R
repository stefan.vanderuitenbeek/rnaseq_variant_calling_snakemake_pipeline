.FILTER_VCF_FUNS_VERSION <- as.numeric_version("0.1.3")

### VarKit ------
install_varkit <- function(version = "0.8.4", upgrade_deps = FALSE, verbose = interactive()) {
  url <- readLines(paste0("https://git.wur.nl/molde006/varkit/-/raw/main/releases/", version))
  message("Installing: ", url)
  install.packages(setdiff("pak", installed.packages()), quiet = !verbose)
  path <- pak::pkg_download(paste0("url::", url), dest_dir = tempdir())$fulltarget
  pak::pkg_install(paste0("deps::", path), upgrade = upgrade_deps)
  install.packages(path, type = "source", repos = NULL, quiet = !verbose)
}

varkit_installed <- function(min_version = "0.8.4", warn = FALSE) {
  if (length(find.package("varkit", quiet = TRUE)) > 0L && utils::packageVersion("varkit") >= min_version) {
    return(TRUE)
  } else if (warn) {
    warning("This script requires package 'varkit' >= v", min_version, "! Please Run `install_varkit()`.")
  }
  return(FALSE)
}


### Main ------
# VCF filtering pipeline, optionally converts AD to Alternative Allele Fractions (AAF).
#  - VCF_PATH string, path of the VCF to be filtered
#  - FILTERS list, containing filters$call$[[dp|bi_frac|ad_cap]][[min|max]]
#            filters$site[[STATISTIC]][[min|max|na.rm]], all of which are optional
#  - VCF_OUT string, path for saving the resulting filtered vcf (and it's index)
#  - INFO_OUT string, path for saving the information used for filtering as a TSV
#  - SAMPLE_OUT string, path for saving the per sample statistics (based on filtered sites)
#  - COL_SELECT tidyselect expression, columns/samples to load from VCF_PATH.
#               See: https://tidyselect.r-lib.org/reference/language.html
#  - REMOVE_SNPEFF boolean, indicates if snpEff fields should be trimmed from INFO column
#  - CHUNCK_SIZE numeric, number of lines to read at a time
filter_vcf_chunked <- function(vcf_path,
                               filters,
                               vcf_out,
                               info_out,
                               sample_out,
                               dp_out = NULL,
                               col_select = NULL,
                               out_format = "AAF",
                               na_geno = ".",
                               remove_snpEff = FALSE,
                               chunk_size = 1e4
                               ) {
  require(tidyverse)
  require(varkit)

  # Checks
  stopifnot(is.null(out_format) || rlang::is_scalar_character(out_format))
  if (!is.null(dp_out) && !identical(out_format, "AAF"))
    stop("The `dp_out` argument is only supported when `out_format = 'AAF'`!")
  stopifnot(rlang::is_bool(remove_snpEff))
  stopifnot(rlang::is_scalar_integerish(chunk_size))
  .validate_vcf_filters(filters, vcf_path = vcf_path, out_format = out_format)

  # If not specified otherwise, only keep true variants in VCF_OUT
  if (is.null(filters$site$true_variant$min))
    filters$site$true_variant$min <- 1L

  # Get the VCF index, creating one if needed
  if (is_varkit_index(vcf_path)) {
    index <- vcf_path
  } else {
    index <- get_vcf_index(vcf_path, chunk_size = chunk_size, col_select = {{ col_select }})
  }

  # Rebuild the index such that it respects CHUNK_SIZE
  index <- as_chunked_index(index, chunk_size = chunk_size)
  on.exit(close(index))

  # Loop over the VCF in chunks and apply filters
  sample_info <- .filter_vcf_chunked_internal(
    index = index, filters = filters, vcf_out = vcf_out, info_out = info_out, dp_out = dp_out,
    out_format = out_format, na_geno = na_geno, remove_snpEff = remove_snpEff
  )

  cli::cli_progress_step("Collecting per sample statistics")
  sample_info %>%
    summarise(across(everything(), sum), .by = sample) %>%
    mutate(across(starts_with("sum_"), function(x) x / n_sites)) %>%
    rename_with(function(x) str_replace(x, "^sum_", "mean_")) %>%
    write_tbl(sample_out, digits = 6)
}

.filter_vcf_chunked_internal <- function(index,
                                               filters,
                                               vcf_out,
                                               info_out,
                                               dp_out,
                                               out_format,
                                               na_geno,
                                               remove_snpEff) {
  # Open output files
  vcf_out <- file_con(vcf_out, "w")
  on.exit(close(vcf_out), add = FALSE)
  info_out <- file_con(info_out, "w")
  on.exit(close(info_out), add = TRUE)
  if (!is.null(dp_out)) {
    dp_out <- file_con(dp_out, "w")
    on.exit(close(dp_out), add = TRUE)
  }

  # Flags used within the loop
  info_written <- FALSE
  vcf_written <- FALSE

  # Loop trough the VCF body in chunks to save memory
  sample_stats <- apply_index(index, function(vcf) {
    # Make sure we are using all sites
    vcf <- unmask_all(vcf)
    n_site_chunk <- n_sites(vcf)

    if (remove_snpEff)
      vcf <- .remove_snp_eff(vcf, skip_meta = vcf_written)

    # Parse the INFO & QUAL columns and move them to a separate df
    info <- tibble()
    if ("info" %in% colnames(vcf$site)) {
      info <- parse_info_all(vcf, use_meta = TRUE)
      colnames(info) <- tolower(colnames(info))
      vcf$site$info <- NULL
    }
    if ("qual" %in% colnames(vcf$site)) {
      info$qual <- vcf$site$qual
      vcf$site$qual <- NULL
    }
    if (!any(colnames(info) == "indel")) {
      info$indel <- is_indel(vcf)
    }

    # REVIEW special methods for GT?
    if (identical(out_format, "AAF")) {
      # Compute bi-allelic Alternative Allele Fraction (AAF) and apply filters
      calls_obj <- .make_calls_aaf(vcf, filters = filters)
      info <- .collect_info_aaf(info, calls_obj, min_bi_frac = filters$call$bi_frac$min)
    } else {
      calls_obj <- .make_calls_default(vcf, filters = filters, out_format = out_format)
      info <- .collect_info_default(info, calls_obj)
    }

    # Keep sites that pass ALL of the cutoffs
    info$pass <- apply_filters(info, cutoffs = filters$site) == "PASS"

    # Write site info to file
    site(vcf, c("chrom", "pos", "ref", "alt")) %>%
      bind_cols(info) %>%
      write_tbl(info_out, append = info_written, digits = 6)
    info_written <<- TRUE

    if (any(info$pass)) {
      # Remove all sites which do not pass the filters
      if (!all(info$pass)) {
        idx <- which(info$pass)
        calls_obj <- .slice_calls(calls_obj, idx = idx)
        vcf <- new_varkit(
          meta = vcf$meta,
          site = vcf$site[idx, ],
          geno = calls_obj[[1L]],
          use_site = rep(TRUE, length(idx))
        )
      } else {
        geno(vcf) <- calls_obj[[1L]]
      }

      # Adjust FROMAT field if changed
      if (!is.null(out_format))
        vcf$site$format <- out_format

      if (identical(out_format, "AAF")) {
        vcf <- .reformat_vcf_aaf(vcf, alt_idx = with(info, alt_idx[pass]), filters, skip_meta = vcf_written)
        sample_stats <- .collect_sample_stats_aaf(calls_obj, min_bi_frac = filters$call$bi_frac$min)
      } else {
        sample_stats <- .collect_sample_stats_default(calls_obj)
      }

      if (!is.null(dp_out)) {
        site(vcf, c("chrom", "pos", "ref", "alt")) %>%
          bind_cols(as.data.frame(t(calls_obj$bi_dp))) %>%
          write_tbl(file = dp_out, append = vcf_written)
      }

      # Write filtered VCF
      suppressMessages(write_vcf(vcf,
        file = vcf_out,
        append = vcf_written,
        na_geno = na_geno,
        digits = 3
      ))
      vcf_written <<- TRUE

      # Return the current chunks sample statistics
      sample_stats
    }
  })
  do.call(rbind, sample_stats)
}

as_chunked_index <- function(index, chunk_size = 1e4) {
  require(varkit)
  stopifnot(is_varkit_index(index), c("linum", "n_lines") %in% colnames(index))

  eof <- with(tail(index, n = 1L), linum + n_lines)
  n_chunks <- ceiling((eof - index$linum[1L]) / chunk_size)

  new_index <- new_varkit_index(tibble::tibble(
    id = rep("placeholder", n_chunks),
    linum = seq(index$linum[1L], eof - 1L, by = chunk_size),
    n_lines = diff(c(linum, eof))
  ))

  attr(new_index, "env") <- attr(index, "env")
  new_index
}

### snpEff ------
# Remove information added to the VCF by the snpEff tool
.remove_snp_eff <- function(vcf, skip_meta = FALSE) {
  # Remove from header
  if (!skip_meta) {
    vcf$meta <- vcf$meta %>%
      filter(name != "INFO" | !str_starts(value, "^<ID=(ANN|LOF|NMD),"))
  }

  # Remove from INFO
  if ("info" %in% colnames(vcf$site))
    vcf$site$info <- str_remove_all(vcf$site$info, "(?:^|;)(?:ANN|LOF|NMD)=[^;\\t]*")

  vcf
}

### Parse FROMAT ------
## Default ------
.make_calls_default <- function(vcf, filters, out_format) {
  dp <- parse_geno(vcf, type = "DP")

  if (!is.numeric(dp))
    stop("Failed to correctly parse DP field from geno(vcf)!")

  # Apply filters by setting values to 0
  dp_min <- filters$call$dp$min
  dp_max <- filters$call$dp$max

  remove <- FALSE
  if (!is.null(dp_min))
    remove[dp < dp_min] <- TRUE
  if (!is.null(dp_max))
    remove[dp > dp_max] <- TRUE

  remove <- which(remove)
  dp[remove] <- 0L

  if (identical(out_format, "DP")) {
    return(list(dp = dp))
  }

  # Combine desired results into a list and return
  if (is.null(out_format)) {
    calls <- geno(vcf)
  } else {
    calls <- parse_geno(vcf, type = out_format)
    calls[remove] <- NA
  }

  list(calls = calls, dp = dp)
}

.collect_info_default <- function(info, calls_obj) {
  stopifnot(is.data.frame(info))
  stopifnot(is.list(calls_obj))

  info$true_variant <- matrixStats::colAnys(calls_obj$dp != 0L)
  info$new_dp <- colSums(calls_obj$dp)
  info$frac_missing <- colMeans(calls_obj$dp == 0L)
  info$mean_dp <- info$new_dp / nrow(calls_obj$dp) / (1 - info$frac_missing)

  info
}

.collect_sample_stats_default <- function(calls_obj) {
  stopifnot(is.list(calls_obj))

  tibble(
    sample = rownames(calls_obj[[1L]]),
    n_sites = rowSums(calls_obj$dp != 0L),
    sum_dp = rowSums(calls_obj$dp)
  )
}

## Alternative Allele Fractions (AAF) ------
# Compute bi-allelic Alternative Allele Fraction (AAF) and apply filters
.make_calls_aaf <- function(vcf, filters, type = "AD", ...) {
  aaf_obj <- varkit::ad2aaf(vcf, ad_cap = filters$call$ad_cap$max, type = type, ...)

  dp_min <- filters$call$dp$min
  dp_max <- filters$call$dp$max
  frac_min <- filters$call$bi_frac$min

  remove <- FALSE
  if (!is.null(dp_min))
    remove[aaf_obj$bi_dp < dp_min] <- TRUE
  if (!is.null(dp_max))
    remove[aaf_obj$bi_dp > dp_max] <- TRUE
  if (!is.null(frac_min))
    remove[aaf_obj$bi_frac < frac_min] <- TRUE

  if (any(remove)) {
    aaf_obj$aaf[remove] <- NA_real_
    aaf_obj$bi_dp[remove] <- 0L
  }

  aaf_obj
}

.collect_info_aaf <- function(info, aaf_obj, min_bi_frac = NULL) {
  stopifnot(is.data.frame(info))
  stopifnot(is.list(aaf_obj))

  # Subsetting of samples or filtering of individual calls in previous steps might
  # have removed all ALT reads from specific sites, these are not 'true variants'
  info$true_variant <- matrixStats::colAnys(aaf_obj$aaf > .Machine$double.eps, na.rm = TRUE)

  # Compute additional statistics
  info$alt_idx <- aaf_obj$alt_idx
  info$new_dp <- colSums(aaf_obj$bi_dp)
  info$mean_dp <- replace_na(info$new_dp / colSums(aaf_obj$bi_dp != 0L), 0)
  info$frac_missing <- colMeans(is.na(aaf_obj$aaf))
  if (is.null(min_bi_frac)) {
    info$n_multi_allelic <- NA
  } else {
    info$n_multi_allelic <- colSums(aaf_obj$bi_frac < min_bi_frac, na.rm = TRUE)
  }

  # Minor Allelic Depth (high quality reads)
  # Alternative allele != minor allele (in some cases)
  info$minor_ad <- as.integer(round(colSums(aaf_obj$aaf * aaf_obj$bi_dp, na.rm = TRUE)))
  info$minor_ad[is.na(info$minor_ad)] <- 0L
  ref_is_minor <- which(info$minor_ad * 2L > info$new_dp)
  if (length(ref_is_minor) > 0L)
    info$minor_ad[ref_is_minor] <- info$new_dp[ref_is_minor] - info$minor_ad[ref_is_minor]

  # REVIEW MAC filter? This requires setting a threshold for when an allele is
  # considered present (& requires ploidy)

  info
}

# Convert the VCF format to AAF
.reformat_vcf_aaf <- function(vcf, alt_idx, filters, skip_meta) {
  # Replace the header
  if (!skip_meta) {
    aaf_format <- .paste(
      '<ID=AAF,Number=1,Type=Float,Description="Alt Allele Frequency (bi-allellic);',
      'AD cap = ', filters$call$ad_cap$max, ';',
      filters$call$dp$min, '<= bi-allelic DP <= ', filters$call$dp$max, ';',
      'fraction bi-allelic >= ', filters$call$bi_frac$min, '">'
    )

    meta(vcf) <- bind_rows(
      filter(meta(vcf) , name != "FORMAT"),
      tibble(name = "FORMAT", value = aaf_format)
    )
  }

  # Convert ALT column to reflect bi-allelic conversion made when using AAF
  collapse_alt(vcf, alt_idx = alt_idx)
}

.has_aaf <- function(vcf_header) {
  any(startsWith(dplyr::filter(meta(vcf_header), name == "FORMAT")$value, "<ID=AAF,"))
}

.collect_sample_stats_aaf <- function(aaf_obj, min_bi_frac) {
  stopifnot(is.list(aaf_obj))

  stats <- tibble(
    sample = rownames(aaf_obj$aaf),
    n_sites = rowSums(!is.na(aaf_obj$aaf)),
    sum_dp = rowSums(aaf_obj$bi_dp, na.rm = TRUE)
  )

  if (is.null(min_bi_frac)) {
    stats$n_multi_allelic <- NA
  } else {
    stats$n_multi_allelic <- rowSums(aaf_obj$bi_frac < min_bi_frac, na.rm = TRUE)
  }

  stats
}

### Filtering ------
.check_cutoffs <- function(cutoffs, available_cols, skip = NULL) {
  if (is.null(cutoffs)) {
    return(TRUE)
  } else if (!is.list(cutoffs) || length(cutoffs) == 0L) {
    stop(if (is.null(prefix)) substitute(cutoffs) else prefix, "is malformed!")
  }
  stopifnot(is.character(available_cols))
  if (!is.null(skip))
    stopifnot(is.character(skip))

  actions <- c("min", "max", "na.rm")
  checks <- c("is.available", "has.action", "na.rm", "single.length", "min.is.numeric", "max.is.numeric", "min.lte.max")

  # Matrix to store check results
  results <- matrix(NA, length(cutoffs), length(checks))
  rownames(results) <- names(cutoffs)
  colnames(results) <- checks

  # Run checks and fill matrix
  for (i in seq_along(cutoffs)) {
    iname <- names(cutoffs)[i]
    if (iname %in% skip)
      next()
    results[i, "is.available"] <- iname %in% available_cols

    if (length(cutoffs[[i]]) == 0L)
      next()
    results[i, "has.action"] <- any(actions %in% names(cutoffs[[i]]))

    if (!is.null(cutoffs[[i]]$na.rm))
      results[i, "na.rm"] <- isTRUE(cutoffs[[i]]$na.rm)

    min <- cutoffs[[i]]$min
    max <- cutoffs[[i]]$max
    if (is.null(min) && is.null(max))
      next()

    results[i, "single.length"] <- length(min) <= 1L && length(max) <= 1L
    if (!is.null(min))
      results[i, "min.is.numeric"] <- is.numeric(min)
    if (!is.null(max))
      results[i, "max.is.numeric"] <- is.numeric(max)
    if (isTRUE(all(results[i, c("min.is.numeric", "max.is.numeric")])))
      results[i, "min.lte.max"] <- isTRUE(min <= max)
  }

  results
}

.validate_vcf_filters <- function(filters, vcf_path, out_format) {
  if (is.null(filters))
    return(invisible(TRUE))
  stopifnot(is.list(filters))

  if (!is.null(filters$call) && identical(out_format, "AAF")) {
    call_checks <- .check_cutoffs(filters$call, available_cols = c("dp", "bi_frac", "ad_cap"))
    if ("bi_frac" %in% rownames(call_checks) && !is.na(call_checks["bi_frac", "max.is.numeric"]))
      warning("The entry '", substitute(filters), "$call$bi_frac$max' is provided but ignored.")
    if ("ad_cap" %in% rownames(call_checks) && !is.na(call_checks["ad_cap", "min.is.numeric"]))
      warning("The entry '", substitute(filters), "$call$ad_cap$min' is provided but ignored.")

    if (any(!call_checks, na.rm = TRUE)) {
      .print_stderr(call_checks)
      stop("The entry '", substitute(filters), "$call' has not passed all checks! See above matrix.")
    }
  }

  if (is.null(filters$site))
    return(invisible(TRUE))

  # Data derived from the INFO columns
  info_cols <- tolower(filter(
    parse_meta(read_vcf_header(vcf_path), "INFO"),
    Number %in% c("0", "1"),
    Type %in% c("Flag", "Integer", "Float")
  )$ID)

  # INFO columns generated by filter_sites_chunked()
  info_cols <- c(info_cols,
    "qual", "indel", "true_variant", "frac_missing", "new_dp", "mean_dp",
    if (identical(out_format, "AAF")) c("n_multi_allelic", "minor_ad")
  )

  site_checks <- .check_cutoffs(filters$site, available_cols = info_cols)
  if (any(!site_checks, na.rm = TRUE)) {
    .print_stderr(site_checks)
    stop("The entry '", substitute(filters), "$site' has not passed all checks! See above matrix.")
  }

  invisible(TRUE)
}

apply_filters <- function(df, cutoffs) {
  stopifnot(is.data.frame(df))
  if (!is.null(cutoffs))
    stopifnot(is.list(cutoffs))

  res <- lapply(names(cutoffs), function(current) {
    curr_res <- character(nrow(df))
    min <- cutoffs[[current]]$min
    if (!is.null(min))
      curr_res[df[[current]] < min] <- paste0(";", current, "<", min)

    max <- cutoffs[[current]]$max
    if (!is.null(max))
      curr_res[df[[current]] > max] <- paste0(";", current, ">", max)

    na_rm <- cutoffs[[current]]$na.rm
    if (isTRUE(na_rm) && anyNA(df[[current]]))
      curr_res[is.na(df[[current]])] <- paste0(";", current, "==NA")

    curr_res
  })
  res <- do.call(paste0, res)

  # Remove the leading ';'
  res <- stringr::str_sub(res, start = 2)
  res[res == ""] <- "PASS"
  res
}

### Col classes ------
info_col_classes <- function(vcf_path) {
  require(varkit)

  # Obtain INFO classes from VCF header and convert to lower case
  header <- read_vcf_header(vcf_path)
  info_classes <- get_info_col_types(header)
  names(info_classes) <- tolower(names(info_classes))

  # Additional classes depending on the output FORMAT
  additional_classes <- if (.has_aaf(header))
    c(alt_idx = "i", n_multi_allelic = "i", minor_ad = "i")

  # Combine with classes that are always present
  c(
    # Fixed VCF classes
    chrom = "c", pos = "i", ref = "c", alt = "c", qual = "d",
    # Required classes
    true_variant = "l", pass = "l", new_dp = "i", mean_dp = "d", frac_missing = "d",
    # Variable classes classes
    info_classes, additional_classes
  )
}

sample_col_classes <- function(vcf_path) {
  c(sample = "c", .default = "d")
}

### Utility ------
# Wrapper for paste() that makes NULL values explicit
.paste <- function(..., sep = "", collapse = NULL, null = "NULL") {
  args <- list(...)
  args[sapply(args, is.null)] <- null
  do.call(paste, c(args, list(sep = sep, collapse = collapse)))
}

# Print an object to stderr() instead of stdout()
.print_stderr <- function(x) {
  message(paste(capture.output(x), collapse = "\n"))
}

# Used to extract specific sites from the result returned by ad2aaf()
.slice_calls <- function(calls_obj, idx) {
  stopifnot(is.list(calls_obj))

  if (is.logical(idx)) {
    if (all(idx))
      return(calls_obj)
    idx <- which(idx)
  }

  calls_obj[!map_lgl(calls_obj, is.matrix)] <- NULL
  for (i in seq_along(calls_obj)) {
    calls_obj[[i]] <- calls_obj[[i]][, idx, drop = FALSE]
  }

  calls_obj
}

### Auto-update ------
update_filter_vcf_functions <- function(path = NULL, check = TRUE) {
  url <- "https://git.wur.nl/stefan.vanderuitenbeek/dnaseq_variant_calling_snakemake_pipeline/-/raw/main/workflow/scripts/r/filter_vcf_functions.R"

  if (is.null(path))
    stop("Please provide the 'path' to the script file you want updated!")

  if (isTRUE(check)) {
    if (!file.exists(path))
      stop("The provided 'path' doesn't exist: ", path)

    header <- readLines(path, n = 1)
    if (!startsWith(header, ".FILTER_VCF_FUNS_VERSION")) {
      stop(
        "The provided 'path' doesn't appear to be the original script file!\n",
        "Please use `check = FALSE` if you still want to overwrite the following file:\n", path
      )
    }
  }

  temp_file <- file.path(tempdir(TRUE), "filter_vcf_functions.latest.R")
  download.file(url, destfile = temp_file, quiet = TRUE)
  if (file.size(temp_file) < 1000)
    stop("Failed to download latest script version from: ", url)

  file.rename(temp_file, path)
  invisible(path)
}

require_filter_vcf_version <- function(min_version) {
  if (!isTRUE(.FILTER_VCF_FUNS_VERSION >= as.numeric_version(min_version)))
    stop("The version of 'filter_vcf_functions' should be >= ", min_version, "! Please call `update_filter_vcf_functions()`.", call. = FALSE)
  invisible(TRUE)
}

### Main ------
# Report the script version
message("Successfully loaded 'filter_vcf_functions' v", .FILTER_VCF_FUNS_VERSION)

# Check if the required version of VarKit is installed
invisible(varkit_installed(warn = TRUE))
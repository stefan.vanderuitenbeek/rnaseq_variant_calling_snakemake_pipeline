#!/usr/bin/env python3

import argparse
import gzip

# import ipdb
# ipdb.set_trace()

def open_file(path, mode = "rt"):
    if mode.startswith("w") and path.endswith(".bgz"):
        raise Exception("Writing to Blocked-Gzip format is not supported!")
    if path.endswith(".gz") or path.endswith(".bgz"):
        return gzip.open(path, mode)
    return open(path, mode)


def parse_header(in_file, out_file = None, skip_meta = False):
    if out_file is None:
        skip_meta = True

    for line in in_file:
        if line.startswith("##"):
            if not skip_meta:
                out_file.write(line)
            continue
        if line.startswith("#"):
            return line[1:].strip().split("\t")
        raise Exception("Non meta line detected in header")


def which_fixed_col(col_names):
    fixed_cols = ["CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "FORMAT"]
    return { el: idx for idx, el in enumerate(col_names) if el in fixed_cols }


def get_idx(list, idx):
    return [ list[i] for i in idx ]


def make_rowname(fields, rowname_cols):
    lst = get_idx(fields, rowname_cols)
    return f"{lst[0]}:{lst[1]}-{lst[2]}/{lst[3]}"


def write_line(file, rowname, values, sep = "\t"):
    if not isinstance(values, list):
        values = list(values)
    file.write(sep.join([rowname] + values) + "\n")


def vcf2genmat(in_path, out_path, skip_meta = False):
    in_file = open_file(in_path)
    out_file = open_file(out_path, "wt")

    # Parse colnames and write header to the fixed file
    col_names = parse_header(in_file, out_file, skip_meta)

    # Find indexes of specific columns
    which_fixed = which_fixed_col(col_names)
    geno_start = max(which_fixed.values()) + 1
    rowname_cols = [ which_fixed[el] for el in ["CHROM", "POS", "REF", "ALT"] ]

    if len(rowname_cols) != 4:
        raise Exception("Not all required columns are present! Is the VCF valid?")

    # Write headers
    rowname = make_rowname(col_names, rowname_cols)
    write_line(out_file, rowname, col_names[geno_start:])

    # Parse remaineder
    for line in in_file:
        fields = line.strip().split("\t")
        rowname = make_rowname(fields, rowname_cols)
        write_line(out_file, rowname, fields[geno_start:])

    in_file.close()
    out_file.close()


if __name__ == "__main__":
    # Create CLI
    parser = argparse.ArgumentParser()
    parser.add_argument("input",
                        help="Input file name, zipped files should end with .gz or .bgz")
    parser.add_argument("output",
                        help="Genotype matrix file name (TSV), zipped files should end with .gz")
    parser.add_argument("-s", "--skip", action="store_true",
                        help="Skip printing header lines starting with #")
    xargs = parser.parse_args()

    vcf2genmat(xargs.input, xargs.output, xargs.skip)

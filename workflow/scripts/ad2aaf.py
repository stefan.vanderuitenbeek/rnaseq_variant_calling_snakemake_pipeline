#!/usr/bin/env python3

# Disclaimer: only works for diploid organisms and biallelic sites

import argparse
import gzip

# import ipdb
# ipdb.set_trace()

## Utility functions
def cat(x):
    print(x, end = "")

def as_int(lst):
    return [ int(x) for x in lst ]

def which_max(lst):
    return lst.index(max(lst))

def first_match_idx(list, what, ensure = False):
    for i, x in enumerate(list):
        if x == what:
            return i
    if ensure:
        raise Exception(f"Not match for '{what}'")
    return None

## Vcf specific functions
def open_vcf(vcf_path):
    if vcf_path.endswith(".vcf"):
        return open(vcf_path, "rt")
    elif vcf_path.endswith(".vcf.gz") or vcf_path.endswith(".vcf.bgz"):
        return gzip.open(vcf_path, "rt")
    else:
        raise Exception(f"Extension not supported for reading: {vcf_path}")

def parse_header(vcf, add = None):
    for line in vcf:
        if line.startswith("##"):
            cat(line)
            continue
        # Header should end after this line
        if line.startswith("#"):
            if add is not None:
                print(add)
            cat(line)
            colnames = line[1:].strip().split("\t")
            return colnames
        raise Exception("Non header line detected in header")

def get_info(info, what):
    for field in info.split(";"):
        if field.startswith(what + "="):
            return field.partition("=")[2]
    raise Exeption(f"The key '{what}' could not be found in the INFO column!")

def which_max_ac(info):
    ac = as_int(get_info(info, "AC").split(","))
    return which_max(ac)

def convert_geno(geno, ad_idx, alt_idx, min_read_depth, min_frac_biallelic):
    new_geno = []
    for genotype in geno:
        # Find read depth for reference and most common alternative allele
        AD = as_int(genotype.split(":")[ad_idx].split(","))
        n_ref = AD[0]
        n_alt = AD[alt_idx + 1]
        bi_sum = n_ref + n_alt

        # Calls below quality tresholds become NA ('.')
        if bi_sum < min_read_depth or bi_sum / sum(AD) < min_frac_biallelic:
            new_geno.append(".")
            continue

        # Calculate alternative allele fraction
        AAF = round(n_alt / bi_sum, 2)
        new_geno.append(str(AAF))
    return new_geno

def is_informative(geno):
    for call in geno:
        if call != "." and call != "0.0":
            return True
    return False

## Main function
def ad2aaf(vcf_path, min_read_depth = 4, min_frac_biallelic = 0.95, remove_info = False):
    if not 0 < min_frac_biallelic <= 1:
        raise Exception("Argument `min_frac_biallelic` does not seem to be a fraction")

    vcf = open_vcf(vcf_path)

    # Parse header lines and write colnames to out file
    format_line = f"##FORMAT=<ID=AAF,Number=1,Type=Float,Description=\"Alt Allele Frequency (bi-allellic);bi-allelic dp >= {min_read_depth};frac bi-allelic >= {min_frac_biallelic}\">"
    colnames = parse_header(vcf, add = format_line)

    # Parse the rest of the file
    for line in vcf:
        splits = line.strip().split("\t")

        # Select the most common alt allele if there are multiple
        alt = splits[4].split(",")
        if len(alt) > 1:
            # REVIEW: highest AC != highest sum(AD) in some cases
            alt_idx = which_max_ac(splits[7])
        else:
            alt_idx = 0
        splits[4] = alt[alt_idx]

        # Get the index of the AD field
        format = splits[8].split(":")
        ad_idx = first_match_idx(format, "AD", ensure = True)

        # Convert biallelic genotype fields to alternative allele fractions using the AD field
        geno = convert_geno(splits[9:], ad_idx, alt_idx, min_read_depth, min_frac_biallelic)

        # Do not print a site if it is no longer informative after conversion
        if is_informative(geno):
            if remove_info:
                splits[7] = "."

            # Write new line replacing FORMAT
            print("\t".join(splits[:8] + ["AAF"] + geno))

    vcf.close()

if __name__ == "__main__":
    # Create CLI
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="Input VCF file ending in .vcf, .vcf.gz, or .vcf.bgz")
    parser.add_argument("-d", dest="min_depth_biallelic", default=4, type=int,
                        help="Minimum bi-allelic read depth; Calls below this value become NA (\".\")")
    parser.add_argument("-f", dest="min_frac_biallelic", default=0.95, type=float,
                        help="Minimum fraction of reads that should be bi-allelic; Calls below this value become NA (\".\")")
    parser.add_argument("-r", dest="remove_info", action="store_true",
                        help="Remove the info field to save space")
    xargs = parser.parse_args()

    ad2aaf(xargs.input, xargs.min_depth_biallelic, xargs.min_frac_biallelic, xargs.remove_info)

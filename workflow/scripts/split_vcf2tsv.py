#!/usr/bin/env python3

import argparse
import gzip

# import ipdb
# ipdb.set_trace()

def open_file(path, mode = "rt"):
    if mode.startswith("w") and path.endswith(".bgz"):
        raise Exception("Writing to Blocked-Gzip format is not supported!")
    if path.endswith(".gz") or path.endswith(".bgz"):
        return gzip.open(path, mode)
    return open(path, mode)

def parse_header(in_file, out_file, skip_meta = False):
    for line in in_file:
        if line.startswith("##"):
            if not skip_meta:
                out_file.write(line)
            continue
        if line.startswith("#"):
            return line[1:]
        raise Exception("Non meta line detected in header")

def split_line(line):
    splits = line.split("\t")
    fixed = "\t".join(splits[:9]) + "\n"
    # Add contig position to genotype matrix
    geno = "\t".join([":".join(splits[0:2])] + splits[9:])
    return [fixed, geno]

def split_vcf2tsv(in_path,
                  fixed_path = "fixed.tsv.gz",
                  geno_path = "genotypes.tsv.gz",
                  skip_meta = False):
    in_file = open_file(in_path)
    fixed_file = open_file(fixed_path, "wt")
    geno_file = open_file(geno_path, "wt")

    # Parse colnames and write header to the fixed file
    col_names = parse_header(in_file, fixed_file, skip_meta)

    # Write headers
    splits = split_line(col_names)
    fixed_file.write(splits[0])
    geno_file.write(splits[1])

    # Parse remaineder
    for line in in_file:
        splits = split_line(line)
        fixed_file.write(splits[0])
        geno_file.write(splits[1])

    in_file.close()
    fixed_file.close()
    geno_file.close()

if __name__ == "__main__":
    # Create CLI
    parser = argparse.ArgumentParser()
    parser.add_argument("input",
                        help="Input file name, zipped files should end with .gz or .bgz")
    parser.add_argument("-f", dest="fixed_out", default="fixed.tsv.gz",
                        help="Fixed output file name (TSV), zipped files should end with .gz or .bgz")
    parser.add_argument("-g", dest="geno_out", default="genotypes.tsv.gz",
                        help="Genotype output file name (TSV), zipped files should end with .gz or .bgz")
    parser.add_argument("-s", "--skip", action="store_true",
                        help="Skip printing header lines starting with #")
    xargs = parser.parse_args()

    split_vcf2tsv(xargs.input, xargs.fixed_out, xargs.geno_out, xargs.skip)
